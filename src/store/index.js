import { createStore } from "vuex";
import card from "./modules/card";
import nav from "./modules/nav";
import steps from "./modules/steps";
import callToAction from "./modules/callToAction";
import mission from "./modules/mission";
import respondentInfo from "./modules/respondentInfo";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: { card, nav, steps, callToAction, mission, respondentInfo },
});
