export default {
  namespaced: true,
  state: {
    callToActionContent: {
      title: "Стань нашим респондентом",
      description:
        "<p class='description-text'> Мы хотим услышать твое мнение, опыт и боли общения с цифровыми продуктами Beeline.</p>",
      buttonText: "Я готов!",
    },
  },
  getters: {
    callToActionContent: (state) => state.callToActionContent,
  },
  mutations: {},
  actions: {},
  modules: {},
};
