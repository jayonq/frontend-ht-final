import api from "../../api/beeline.js";

export default {
  namespaced: true,
  state: {
    benefitsTitle: "Какие плюсы?",
    cards: [],
  },
  getters: {
    cards: (state) => state.cards,
    benefitsTitle: (state) => state.benefitsTitle,
  },
  mutations: {
    setCards: (state, payload) => {
      state.cards = payload;
    },
  },
  actions: {
    async fetchBenefits({ commit }) {
      const response = await api.fetchBenefits();
      commit("setCards", response);
    },
  },
  modules: {},
};
