import api from "../../api/beeline.js";

export default {
  namespaced: true,
  state: {
    stepsTitle: "Как принять участие?",
    steps: [],
  },
  getters: {
    steps: (state) => state.steps,
    stepsTitle: (state) => state.stepsTitle,
  },
  mutations: {
    setSteps: (state, payload) => {
      state.steps = payload;
    },
  },
  actions: {
    async fetchSteps({ commit }) {
      const response = await api.fetchSteps();
      commit("setSteps", response);
    },
  },
  modules: {},
};
