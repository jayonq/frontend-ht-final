export default {
  namespaced: true,
  state: {
    missionContent: {
      title: "Наша миссия",
      description:
        "<p class='description-text'>Тебя тоже раздражает, когда ты пользуешься приложением или сайтом и непонятно куда нажать? Когда попадаешь на пустой экран после ввода данных? </p> <p class='description-text'>Нам тоже это не нравится!</p> <p class='description-text'> Мы хотим улучшать наши продукты, делать их проще и удобнее для пользователя. Присоединяйся, без тебя нам не обойтись!</p>",
      buttonText: "Присоедениться",
    },
  },
  getters: {
    missionContent: (state) => state.missionContent,
  },
  mutations: {},
  actions: {},
  modules: {},
};
