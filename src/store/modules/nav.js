export default {
  namespaced: true,
  state: {
    navItems: [
      {
        title: "Какие плюсы?",
        id: "benefits",
      },
      {
        title: "Как принять участие?",
        id: "steps",
      },
      {
        title: "Наша миссия",
        id: "mission",
      },
      {
        title: "Присоединиться",
        id: "mainCta",
      },
    ],
  },
  getters: {
    navItems: (state) => state.navItems,
  },
  mutations: {},
  actions: {},
  modules: {},
};
