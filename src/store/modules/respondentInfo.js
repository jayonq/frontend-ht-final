export default {
  namespaced: true,
  state: {
    respondentInfo: {
      title: "Кто такой респондент?",
      description:
        "Респондент - человек, принимающий участие в опросе, интервью или тестировании дизайна. Мы используем твои ответы в проектировании новых продуктов, чтобы сделать их понятнее, удобнее и приятнее для пользователей.",
    },
  },
  getters: {
    respondentInfo: (state) => state.respondentInfo,
  },
  mutations: {},
  actions: {},
  modules: {},
};
