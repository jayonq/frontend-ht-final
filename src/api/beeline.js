export default {
  fetchBenefits() {
    const response = [
      {
        imageSrc:
          "https://gitlab.com/jayonq/frontend-ht-final/-/raw/main/src/assets/images/benefit1.png",
        imageAlt: "benefit1",
        title: "Будь первым",
        description:
          "Ты первым увидишь наши новые продукты и поучаствуешь в их разработке",
        backgroundColor: "#fff6cc",
      },
      {
        imageSrc:
          "https://gitlab.com/jayonq/frontend-ht-final/-/raw/main/src/assets/images/benefit2.png",
        imageAlt: "benefit2",
        title: "Поделись опытом",
        description:
          "Будем рады узнать о твоем опыте использования наших продуктов",
        backgroundColor: "#EAF4FF",
      },
      {
        imageSrc:
          "https://gitlab.com/jayonq/frontend-ht-final/-/raw/main/src/assets/images/benefit3.png",
        imageAlt: "benefit3",
        title: "Получи уникальную ачивку",
        description:
          "Блестящая, свеженькая и такая уникальная ачивка в приложении “Мой Билайн”",
        backgroundColor: "#000",
      },
    ];

    return response;
  },

  fetchSteps() {
    const response = [
      {
        number: "1",
        title:
          "Оставь <a href='https://docs.google.com/forms/d/e/1FAIpQLSfm_Yj07urIb_fF0mTRGZgl5DWt5lC2qYd4TngwXWMgMHiYMg/viewform'>заявку</a>",
        description:
          "Заполни форму максимально честно, выбери продукты которые хочешь тестировать",
        backgroundColor: "#eaf4ff",
        color: "#0183d4",
      },
      {
        number: "2",
        title: "Ожидай приглашения на интервью",
        description:
          "Будь на связи :) Мы напишем/позвоним тебе заранее и договоримся об удобном времени. Обычно интервью не занимает больше 30-40 минут.",
        backgroundColor: "#f3ecff",
        color: "#9e66ff",
      },
      {
        number: "3",
        title: "Позаботься об окружении",
        description:
          "Выбери спокойное, тихое место с хорошим интернетом. Обязательно приноси с собой хорошее настроение 😉",
        backgroundColor: "#dff6d9",
        color: "#61ca45",
      },
      {
        number: "4",
        title: "Получи клевую ачивку",
        description:
          "После первого интервью, в приложении Мой Билайн тебе откроется новая ачивка «Яркий пиздюк». Спасибо, что ты с нами!",
        backgroundColor: "#fff7e2",
        color: "#ffbd12",
      },
    ];

    return response;
  },
};
